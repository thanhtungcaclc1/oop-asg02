package uet.oop.bomberman.Sound;

import javax.sound.sampled.*;
import java.io.*;
import java.net.URL;
import javax.swing.*;

public class Tester {
    static Thread th;


    public void playSound() throws LineUnavailableException,IOException,UnsupportedAudioFileException,InterruptedException{
        Clip clip = AudioSystem.getClip();
        URL url = getClass().getResource("theme.wav");
        AudioInputStream ais = AudioSystem.getAudioInputStream(new File(url.getPath()));
        clip.open(ais);
        clip.loop(1);
        // Calculate totalFrames
        long totalFrames = (long)(clip.getFrameLength() * clip.getFormat().getFrameRate());
        Thread.sleep( ( totalFrames* 1000 )); // pause the thread till the sound plays

        System.out.println(clip.getFrameLength());
        System.out.println(clip.getFormat().getFrameRate());
    }
}